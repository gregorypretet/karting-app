function startCountdown(raceDateTime) {
    let countdownInterval;

    function formatNumber(number) {
        return number < 10 ? '0' + number : number;
    }

    function updateCountdown() {
        const now = new Date().getTime();
        const distance = raceDateTime - now;

        if (distance < 0) {
            clearInterval(countdownInterval);
            document.getElementById("countdown").innerHTML = "En attente de résultats...";
            document.getElementById("countdown").style.color = "#777";
            return;
        }

        const days = Math.floor(distance / (1000 * 60 * 60 * 24));
        const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((distance % (1000 * 60)) / 1000);

        document.getElementById("countdown-days").innerHTML = formatNumber(days);
        document.getElementById("countdown-hours").innerHTML = formatNumber(hours);
        document.getElementById("countdown-minutes").innerHTML = formatNumber(minutes);
        ;
    }

    updateCountdown(); // initial call to avoid delay
    countdownInterval = setInterval(updateCountdown, 1000);
}

