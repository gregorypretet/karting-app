window.addEventListener('load', function () {
  // Récupérer l'URL actuelle
  var currentUrl = window.location.href;

  // Récupérer tous les liens <a> dans la page
  var links = document.querySelectorAll('a');

  // Parcourir tous les liens et vérifier si leur attribut href est égal à l'URL actuelle
  for (var i = 0; i < links.length; i++) {
    if (
      links[i].href === currentUrl &&
      links[i].classList.contains('colorable-link')
    ) {
      // Appliquer la classe active-link si le lien correspond à l'URL actuelle
      links[i].classList.add('active-link');
    }
  }
});
