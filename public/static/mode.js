const body = document.body;
const system = document.getElementById("system-button");
const light = document.getElementById("light-button");
const dark = document.getElementById("dark-button");
const toggle = document.getElementById("toggle-mode");

system.addEventListener("click", () => setTheme("system"));

light.addEventListener("click", () => setTheme("light"));

dark.addEventListener("click", () => setTheme("dark"));

const availableThemes = ["system", "light", "dark"];

function setTheme(themeToSet) {
    switch (themeToSet) {
        case "system":
            system.style.display = "none";
            dark.style.display = "none";
            light.style.display = "block";
            break;
        case "light":
            system.style.display = "none";
            dark.style.display = "block";
            light.style.display = "none";
            break;
        case "dark":
            system.style.display = "block";
            dark.style.display = "none";
            light.style.display = "none";
            break;
        default:
            break;
    }
    if (!availableThemes.includes(themeToSet)) return;
    window.localStorage.setItem("theme", themeToSet);
    availableThemes.forEach((theme) => {
        if (theme !== themeToSet && body.classList.contains(theme))
            body.classList.remove(theme);
    });
    if (!body.classList.contains(themeToSet)) {
        body.classList.add(themeToSet);
    }
}

(function loadTheme() {
    const theme = window.localStorage.getItem("theme");
    setTheme(theme);
})();

// Fonction pour vérifier si la page est scrollée jusqu'en bas avec une marge de 20px
function isPageScrolledToBottom() {
    return (window.innerHeight + window.scrollY) >= (document.documentElement.scrollHeight - 75);
}

// Événement de défilement
window.addEventListener('scroll', function () {
    if (isPageScrolledToBottom()) {
        toggle.classList.add('hidden'); // Ajoute la classe 'hidden'
    } else {
        toggle.classList.remove('hidden'); // Retire la classe 'hidden'
    }
});
