function setAward(award) {
    // Récupérer tous les éléments avec la classe "award"
    var awards = document.getElementsByClassName('award');

    // Parcourir tous les éléments avec la classe "award"
    for (var i = 0; i < awards.length; i++) {
        // Masquer tous les éléments avec la classe "award"
        awards[i].style.display = 'none';
    }

    // Afficher l'élément spécifié par le paramètre "award"
    var selectedAward = document.getElementsByClassName(award)[0];
    if (selectedAward) {
        selectedAward.style.display = 'flex'; // ou 'inline' ou 'inline-block', selon le cas
    }
}


function changeStat(stat) {
    // Récupérer tous les éléments avec la classe "award"
    var stats = document.getElementsByClassName('stat');

    // Parcourir tous les éléments avec la classe "award"
    for (var i = 0; i < stats.length; i++) {
        // Masquer tous les éléments avec la classe "award"
        stats[i].style.display = 'none';
    }

    // Afficher l'élément spécifié par le paramètre "award"
    var selectedStats = document.getElementsByClassName(stat);
    for (var i = 0; i < selectedStats.length; i++) {
        // Masquer tous les éléments avec la classe "award"
        selectedStats[i].style.display = 'flex';
    }
}